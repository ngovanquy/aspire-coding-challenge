import { createRouter, createWebHashHistory } from 'vue-router';
import { RouteName } from '@/constants/route.constants';

const routes = [
  {
    path: '/',
    redirect: '/code-challenge'
  },
  {
    path: '/css-challenge',
    name: RouteName.CSS_CHALLENGE,
    component: () => import(/* webpackChunkName: "css.challenge" */ '../views/CssChallenge.vue')
  },
  {
    path: '/code-challenge',
    name: RouteName.REGISTER,
    component: () => import(/* webpackChunkName: "code.challenge.register" */ '../views/CodeChallenge.vue')
  },
  {
    path: '/code-challenge/apply-loan',
    name: RouteName.APPLY_LOAN,
    component: () => import(/* webpackChunkName: "code.challenge.apply.loan" */ '../components/features/loan/ApplyLoan.vue')
  },
  {
    path: '/code-challenge/view-payment',
    name: 'ViewPayment',
    component: () => import(/* webpackChunkName: "code.challenge.view.payment" */ '../components/features/loan/ViewPayment.vue')
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;
