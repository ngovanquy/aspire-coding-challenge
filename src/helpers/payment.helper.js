import { PaymentStatus } from '@/constants/app.constants';

export const calculateAccountPaymentStatus = (repayments) => {
  let status = PaymentStatus.UNPAID;
  if (repayments?.length) {
    const isAllRepaid = repayments.every((payment) => payment.isRepaid);
    if (isAllRepaid) {
      status = PaymentStatus.REPAID;
    }
  }

  return status;
};
