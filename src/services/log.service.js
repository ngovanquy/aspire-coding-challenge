export const info = (message) => console.log(message);

export const error = (message) => console.error(message);
