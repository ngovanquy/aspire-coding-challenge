import * as logService from './log.service';
import * as userService from './user.service';
import * as loanService from './loan.service';

export { logService, userService, loanService };
