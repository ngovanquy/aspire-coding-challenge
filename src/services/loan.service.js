import { logService } from '.';

const mockPayments = [
  { id: 1, amount: 10000000, paid: 10000000, loanTerm: 1, status: 'Repaid' },
  { id: 2, amount: 20000000, paid: 0, loanTerm: 3, status: 'Unpaid' },
  { id: 3, amount: 50000000, paid: 0, loanTerm: 6, status: 'Unpaid' }
];

export const applyLoan = ({ userId, amount, loanTerm }) => {
  logService.info(`service ## applyLoan invoked with payload ${JSON.stringify({ userId, amount, loanTerm })}`);
  mockPayments.push({ id: Date.now(), amount, paid: 0, loanTerm, status: 'Unpaid' });
  return Promise.resolve(mockPayments);
};

export const getRepayments = (accountId) => {
  logService.info(`service ## getRepayments invoked with payload ${accountId}`);
  const response = {
    accountId,
    repayments: mockPayments
  };
  return Promise.resolve(response);
};
