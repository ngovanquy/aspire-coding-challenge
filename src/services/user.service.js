import { logService } from '.';

export const createAccount = ({ name, phone, email, address }) => {
  const response = { id: Date.now(), name, phone, email, address };
  logService.info('createAccount response');
  logService.info(response);
  return Promise.resolve(response);
};
