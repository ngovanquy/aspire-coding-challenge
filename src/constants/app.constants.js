export const PaymentStatus = {
  UNPAID: 'Unpaid',
  REPAID: 'Repaid'
};
