export const RouteName = {
  CODE_CHALLENGE: 'CodeChallenge',
  CSS_CHALLENGE: 'CssChallenge',
  REGISTER: 'Register',
  APPLY_LOAN: 'ApplyLoan',
  VIEW_PAYMENT: 'ViewPayment'
}
