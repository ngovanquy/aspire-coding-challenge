# Aspire Coding Challenge

## Project information

- developing with Node 14.18.2
- using Vue 3 + Bulma
- unit tests in *tests\unit*
- 2 code sections mainly divided by features in *components\features\loan* for Code Challenge and *components\features\dashboard* for CSS Challenge 
- unit tests in *tests\unit\components*

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Hosted version
https://nano-bee.herokuapp.com/
