import { shallowMount } from '@vue/test-utils';
import flushPromises from 'flush-promises';
import { RouteName } from '@/constants/route.constants';
import * as userService from '@/services/user.service';
import Register from '@/components/features/loan/Register.vue';

const setupComponent = (props) => {
  const wrapper = shallowMount(Register, {
    props: { ...props }
  });
  return wrapper;
};

describe('Register', () => {
  it('should clear message', () => {
    const wrapper = setupComponent();
    wrapper.vm.clearMessages();
    expect(wrapper.vm.errorMessage).toEqual('');
  });
  describe('validateAccount', () => {
    it('should update error message for name if validation failed', () => {
      const wrapper = setupComponent();

      const result = wrapper.vm.validateAccount();

      expect(wrapper.vm.errorMessage).toEqual('Name is required');
      expect(result).toBe(false);
    });
    it('should update error message for phone if validation failed', () => {
      const wrapper = setupComponent();
      wrapper.vm.name = 'Aspire';

      const result = wrapper.vm.validateAccount();

      expect(wrapper.vm.errorMessage).toEqual('Phone is required');
      expect(result).toBe(false);
    });
    it('should return true if validation passed', () => {
      const wrapper = setupComponent();
      wrapper.vm.name = 'Aspire';
      wrapper.vm.phone = '12345678';

      const result = wrapper.vm.validateAccount();

      expect(wrapper.vm.errorMessage).toEqual('');
      expect(result).toBe(true);
    });
  });
  describe('handleSubmitClick', () => {
    it('should navigate to apply loan page if create account successfully', () => {
      const wrapper = setupComponent();
      wrapper.vm.name = 'Aspire';
      wrapper.vm.phone = '12345678';
      const createAccountSuccessPromise = Promise.resolve();
      const createAccountSpy = jest.spyOn(userService, 'createAccount').mockImplementation(() => createAccountSuccessPromise);
      wrapper.vm.$router = { push: jest.fn() };

      wrapper.vm.handleSubmitClick();

      expect(createAccountSpy).toBeCalled();
      return createAccountSuccessPromise.then(() => {
        expect(wrapper.vm.$router.push).toBeCalledWith({ name: RouteName.APPLY_LOAN });
      });
    });
    it('should set error message if create account failed', async () => {
      const wrapper = setupComponent();
      wrapper.vm.name = 'Aspire';
      wrapper.vm.phone = '12345678';
      const createAccountSpy = jest.spyOn(userService, 'createAccount').mockImplementation(() => Promise.reject({ message: 'oops, create account fail' }));

      wrapper.vm.handleSubmitClick();

      expect(createAccountSpy).toBeCalled();
      await flushPromises();
      expect(wrapper.vm.errorMessage).toEqual('Have issues when creating your account');
    });
  });
});
