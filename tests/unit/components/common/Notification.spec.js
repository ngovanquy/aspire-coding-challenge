import { shallowMount } from '@vue/test-utils';
import Notification from '@/components/common/Notification.vue';

const setupComponent = (props) => {
  const defaultProps = {
    category: ''
  };
  const wrapper = shallowMount(Notification, {
    props: { ...defaultProps, ...props }
  });
  return wrapper;
};

describe('Notification', () => {
  it('should render default css class', () => {
    const wrapper = setupComponent();

    expect(wrapper.vm.cssClass).toBe('is-info');
    expect(wrapper.classes('is-info')).toBe(true);
  });

  it('should render correct css classes for passed category', () => {
    const wrapper1 = setupComponent({ category: 'success' });

    expect(wrapper1.vm.cssClass).toBe('is-success');
    expect(wrapper1.classes('is-success')).toBe(true);

    const wrapper2 = setupComponent({ category: 'error' });

    expect(wrapper2.vm.cssClass).toBe('is-danger');
    expect(wrapper2.classes('is-danger')).toBe(true);
  });
});
